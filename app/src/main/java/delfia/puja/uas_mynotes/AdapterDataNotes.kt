package delfia.puja.uas_mynotes

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main_notes.*

class AdapterDataNotes (val dataNts: List<HashMap<String,String>>, val Notes : main_notes): RecyclerView.Adapter<AdapterDataNotes.HolderDataNotes> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterDataNotes.HolderDataNotes {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.row_notes,parent,false)
        return HolderDataNotes(v)
    }

    override fun getItemCount(): Int {
        return dataNts.size
    }

    override fun onBindViewHolder(holder: AdapterDataNotes.HolderDataNotes, position: Int) {
        val data =dataNts.get(position)
        holder.txnama_notes.setText(data.get("nama_notes"))
        holder.txkat.setText(data.get("nama_kategori"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)

        //if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        //else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            val pos = Notes.daftarkategori.indexOf(data.get("nama_kategori"))
            Notes.spinner.setSelection(pos)
            //mainActivity.spinn.setSelection(pos)
            //mainActivity.txNm.setText(data.get("nim"))
            Notes.id_notes = data.get("id_notes").toString()
            Notes.txNamaNotes.setText(data.get("nama_notes"))
            Notes.id_notes = data.get("id_notes").toString()
            Picasso.get().load(data.get("url")).into(Notes.imageView2)

        })
    }

    class HolderDataNotes(v : View) :  RecyclerView.ViewHolder(v){
        val txnama_notes = v.findViewById<TextView>(R.id.namnot)
        val txkat = v.findViewById<TextView>(R.id.namkat)
        val photo = v.findViewById<ImageView>(R.id.ImgUpload)
        val layouts = v.findViewById<ConstraintLayout>(R.id.rowlay)
    }
}