package delfia.puja.uas_mynotes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.halaman_next.*

class NextActivity : AppCompatActivity(), View.OnClickListener{
    var warnakat = ""
    lateinit var pref : SharedPreferences
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.kk->{
                var inten = Intent(this,login::class.java)
                startActivity(inten)
            }
        }
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.halaman_next)
        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        warnakat = pref.getString("bl","").toString()
        warna()


        kk.setOnClickListener(this)


    }
    fun warna(){

        if (warnakat=="kuning"){
            p.setBackgroundColor(Color.YELLOW)
        }else if(warnakat=="biru"){
            p.setBackgroundColor(Color.BLUE)
        }else if (warnakat=="putih"){
            p.setBackgroundColor(Color.WHITE)
            txHeader.setTextColor(Color.BLACK)
            txby.setTextColor(Color.BLACK)
        }else if(warnakat=="hijau"){
            p.setBackgroundColor(Color.GREEN)
        }

    }

}