package delfia.puja.uas_mynotes

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.main_kategori.*
import org.json.JSONArray
import org.json.JSONObject

class kategori : AppCompatActivity(), View.OnClickListener {
    lateinit var adapkategori: AdapKategori
    var id_kategori: String = ""
    var daftarkategori = mutableListOf<HashMap<String, String>>()
    var uri1 = "http://192.168.43.58/dashboard/uas_cb/show_kategori.php"
    var uri2 = "http://192.168.43.58/dashboard/uas_cb/query_kategori.php"
    var warnakat = ""
    lateinit var pref : SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_kategori)
        adapkategori = AdapKategori(daftarkategori, this)
        lskat.layoutManager = LinearLayoutManager(this)
        lskat.adapter = adapkategori

        pref = getSharedPreferences("a",Context.MODE_PRIVATE)
        warnakat = pref.getString("bl","").toString()
        warna()

        btin.setOnClickListener(this)
        bthap.setOnClickListener(this)
        btup.setOnClickListener(this)

    }

    fun warna(){

        if (warnakat=="kuning"){
            lakat.setBackgroundColor(Color.YELLOW)
        }else if(warnakat=="biru"){
            lakat.setBackgroundColor(Color.BLUE)
        }else if (warnakat=="putih"){
            lakat.setBackgroundColor(Color.WHITE)
        }else if(warnakat=="hijau"){
            lakat.setBackgroundColor(Color.GREEN)
        }

    }


    override fun onStart() {
        super.onStart()
        ShowKat()
    }

    fun ShowKat() {
        val request = StringRequest(Request.Method.POST, uri1, Response.Listener { response ->
            daftarkategori.clear()
            adapkategori.notifyDataSetChanged()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length() - 1)) {
                val jsonObject = jsonArray.getJSONObject(x)
                var kateg = HashMap<String, String>()
                kateg.put("nama_kategori", jsonObject.getString("nama_kategori"))
                kateg.put("id_kategori", jsonObject.getInt("id_kategori").toString())
                daftarkategori.add(kateg)

            }
            adapkategori.notifyDataSetChanged()

        }, Response.ErrorListener { error ->
            Toast.makeText(this, "Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun query(mode: String) {
        val request = object : StringRequest(
            Method.POST, uri2,
            Response.Listener { response ->

                val jsonobject = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowKat()
                } else {
                    Toast.makeText(this, "Operasi Gagal ", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("nama_kategori", namakat.text.toString())
                    }

                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("id_kategori", id_kategori)
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("nama_kategori", namakat.text.toString())
                        hm.put("id_kategori", id_kategori)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btin -> {
                query("insert")


            }
            R.id.bthap -> {
                query("delete")


            }
            R.id.btup -> {
                query("update")
            }
        }
    }
}