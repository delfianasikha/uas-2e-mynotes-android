package delfia.puja.uas_mynotes

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main_notes.*
import org.json.JSONArray
import org.json.JSONObject


class main_notes : AppCompatActivity() , View.OnClickListener {
    var daftarnts = mutableListOf<HashMap<String,String>>()
    var daftarkategori = mutableListOf<String>()
    var imStr = ""
    var namafile = ""
    var pilihkategori =""
    var id_notes : String = ""
    var Fileuri = Uri.parse("")
    lateinit var AdapterDataNotes : AdapterDataNotes
    lateinit var adapkate : ArrayAdapter<String>
    lateinit var  MediaHelperKamera : MediaHelperKamera
    //lateinit var MediaHelper : MediaHelper
    var uri = "http://192.168.43.58/dashboard/uas_cb/show_notes.php"
    var uri1 = "http://192.168.43.58/dashboard/uas_cb/show_kategori.php"
    var ur2 ="http://192.168.43.58/dashboard/uas_cb/query_notes.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_notes)
        AdapterDataNotes =  AdapterDataNotes(daftarnts,this)
        LsNts.layoutManager = LinearLayoutManager(this)
        LsNts.adapter = AdapterDataNotes

        adapkate = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarkategori)
        spinner.adapter = adapkate
        spinner.onItemSelectedListener = itemSelect

        //MediaHelper = MediaHelper(this)

        imageView2.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)


        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        MediaHelperKamera = MediaHelperKamera()


    }
    override fun onStart() {
        super.onStart()
        getKategori()
        ShowNotes()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == MediaHelperKamera.getRcCamera()){
                imStr = MediaHelperKamera.getBitmapToString(imageView2,Fileuri)
                namafile = MediaHelperKamera.getMyfile()
            }
            // if (requestCode == MediaHelper.getRcGalery()){
            //     imStr = MediaHelper.getBitmapToString(data!!.data,imageView2) === ambil dari galery
            // }
        }
    }

    //nampil kategori
    fun getKategori(){
        val request  = StringRequest(
            Request.Method.POST,uri1,
            Response.Listener { response ->
                daftarkategori.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarkategori.add(jsonObject.getString("nama_kategori"))
                }
                adapkate.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->

            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    val itemSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihkategori = daftarkategori.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihkategori = daftarkategori.get(position)
        }

    }


    fun ShowNotes(){
        val request = StringRequest(
            Request.Method.POST,uri, Response.Listener { response ->
                daftarnts.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var no = HashMap<String,String>()
                    no.put("id_notes",jsonObject.getString("id_notes"))
                    no.put("nama_notes",jsonObject.getString("nama_notes"))
                    no.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    no.put("url",jsonObject.getString("url"))
                    daftarnts.add(no)
                }
                AdapterDataNotes.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
            })

        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView2->{
                requestPermition()
            }
            R.id.btnInsert->{
                query("insert")
            }
            R.id.btnDelete->{
                query("delete")

            }
            R.id.btnUpdate->{
                query("update")

            }
        }
    }

/*    //query
    fun query(mode : String){
        val request = object : StringRequest(
            Method.POST,ur2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowNotes()
                    var aa = Intent(this,main_notes::class.java)
                    startActivity(aa)
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                   when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_notes",txNamaNotes.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_kategori",pilihkategori.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_notes",txNamaNotes.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_kategori",pilihkategori)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_notes",txNamaNotes.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
*/

    fun query(mode: String) {
        val request = object : StringRequest(
            Method.POST, ur2,
            Response.Listener { response ->

                val jsonobject = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowNotes()
                } else {
                    Toast.makeText(this, "Operasi Gagal ", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when (mode) {
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_notes",txNamaNotes.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_kategori",pilihkategori.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_notes",id_notes)
                        hm.put("nama_notes",txNamaNotes.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",namafile)
                        hm.put("nama_kategori",pilihkategori)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_notes",id_notes)
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }



    fun requestPermition() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        Fileuri = MediaHelperKamera.getOutputFileUri()

        val inten  = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        inten.putExtra(MediaStore.EXTRA_OUTPUT,Fileuri)
        startActivityForResult(inten,MediaHelperKamera.getRcCamera())
    }
}

