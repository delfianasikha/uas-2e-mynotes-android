package delfia.puja.uas_mynotes

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.SparseLongArray
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.halaman_preference.*

class ColorActivity : AppCompatActivity(),View.OnClickListener{

    val arrayWarna = arrayOf("Blue","Yellow","Green","Black","White")
    lateinit var adapterSpin : ArrayAdapter<String>
    var fHeader : Int = 0
    var bg : String = ""
    lateinit var  pref : SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.halaman_preference)

        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayWarna)
        btnSimpan.setOnClickListener(this)
        btnYellow.setOnClickListener(this)
        btnBlue.setOnClickListener(this)
        btnGreen.setOnClickListener(this)
        btnWhite.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSimpan->{
                pref =  getSharedPreferences("a",Context.MODE_PRIVATE)
                val pj = pref.edit()
                pj.putString("ab",isi.text.toString())
                pj.commit()
                var  pindah = Intent(this,MainActivity::class.java)
                startActivity(pindah)
                Toast.makeText(this,"berhasil di ubah", Toast.LENGTH_LONG).show()

            }
            R.id.btnBlue->{
                pref = getSharedPreferences("a",Context.MODE_PRIVATE)
                val blue = pref.edit()
                blue.putString("bl","biru")
                blue.commit()
                var  pindah = Intent(this,SplassActivity::class.java)
                startActivity(pindah)
                Toast.makeText(this,"berhasil di ubah", Toast.LENGTH_LONG).show()
            }
            R.id.btnGreen->{
                pref = getSharedPreferences("a",Context.MODE_PRIVATE)
                val green = pref.edit()
                green.putString("bl","hijau")
                green.commit()
                var  pindah = Intent(this,SplassActivity::class.java)
                startActivity(pindah)
                Toast.makeText(this,"berhasil di ubah", Toast.LENGTH_LONG).show()

            }
            R.id.btnWhite->{
                pref = getSharedPreferences("a",Context.MODE_PRIVATE)
                val white = pref.edit()
                white.putString("bl","putih")
                white.commit()
                var  pindah = Intent(this,SplassActivity::class.java)
                startActivity(pindah)
                Toast.makeText(this,"berhasil di ubah", Toast.LENGTH_LONG).show()
            }
            R.id.btnYellow->{
                pref = getSharedPreferences("a",Context.MODE_PRIVATE)
                val yellow = pref.edit()
                yellow.putString("bl","kuninga")
                yellow.commit()
                var  pindah = Intent(this,SplassActivity::class.java)
                startActivity(pindah)
                Toast.makeText(this,"berhasil di ubah", Toast.LENGTH_LONG).show()
            }
        }
    }

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fHeader = progress.toFloat().toInt()
        }
        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }
        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }
    }


}