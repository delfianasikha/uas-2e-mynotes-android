package delfia.puja.uas_mynotes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.cardview.widget.CardView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var tmnotes : CardView
    lateinit var tmkategori : CardView
    lateinit var tmcolor : CardView
    lateinit var  pref : SharedPreferences
    var saya = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //deklarasi id card
        tmnotes = findViewById(R.id.tampilNotes)
        tmkategori = findViewById(R.id.tampilKategori)
        tmcolor = findViewById(R.id.tampilColor)


        //set onclik
        tmnotes.setOnClickListener(this)
        tmkategori.setOnClickListener(this)
        tmcolor.setOnClickListener(this)

        pref = getSharedPreferences("a", Context.MODE_PRIVATE)
        saya = pref.getString("ab","My Notes").toString()
        collapbar.title = saya




    }

    //clik
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.tampilNotes->{
                val pindah = Intent(this, main_notes::class.java)
                startActivity(pindah)
            }
            R.id.tampilKategori->{
                val pindah = Intent(this, kategori::class.java)
                startActivity(pindah)
            }
            R.id.tampilColor->{
                val pindah = Intent(this, ColorActivity::class.java)
                startActivity(pindah)
            }


        }
    }




}
